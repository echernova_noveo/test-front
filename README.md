# FRONT END TEST:

## Hi welcome to Wynd front end test:

### Mission:
**Your mission, should you decide to accept it, is to create an app to manage invoices refund.**

*That consists in 3 features:*

* Request an invoice.
* Make a view to get an invoice visualization that contains this sub features:  
    * The possibility to select products you want to refund.
    * The product status visualization. (`DELIVERED` | `REFUNDED`)
    * The action button to send your updated invoice payload.
* Send your updated invoice payload with the calculated new total and changed status. Then repopulate it in your view.

:warning: **There are some compound products (a combo) which contains other products that can be refund individually, you have to calculate a proportional refund between the compound product and the individual product price.**

*For example*: Let's consider a combo that cost `12.00 €` and includes 3 other products that cost each `5.00 €`. If you want to refund just one of them, you have to refund just `4.00€`.

```
3 individual products => 5.00 € * 3 => 15.00€
1 individual product: 5.00 €  =>  1/3 of 15.00 €  
CompoundProduct: 12.00€  =>  1/3 * 12.00 € => 4€
```

:warning: **If you get an invoiced product with a `totalQuantity` of 2 you can ask to refund just one of them so you have to manage the two statuses: `DELIVERED` and `REFUNDED`**

**:art:[Here the graphic view](README/wireframe.jpg)**

### Your stuff:
*You have at your disposal:*

*  A [Json server](https://github.com/typicode/json-server) that provide a REST api with two endpoints :
      *  `http://localhost:9999/products`
      *  `http://localhost:9999/invoices`
* A React App ready to receive all your amazing code. :fire:

All the data are provided from a `db.json` that's copied from `db.json.dist` with the `yarn start`. Each `yarn start` resets the database.
**Launch a `yarn start` and go on an endpoint to see the data structure.**

:warning:**You shouldn't change `db.json.dist` file.**

### Your Stack:
* [Redux](https://redux.js.org/)
* [Redux Saga](https://redux-saga.js.org/)
* [Styled Component](https://www.styled-components.com/)
* [Typescript](https://www.typescriptlang.org/docs/home.html)


---
## INFORMATION:

**FYI**: This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
