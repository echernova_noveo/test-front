import { createGlobalStyle } from 'styled-components';
import { background } from '../const/colors';
import 'reset-css';
import 'typeface-source-sans-pro';

export const GlobalStyle = createGlobalStyle`
  body, html {
    margin: 0;
    padding: 0;
    background-color: ${background};
    font-family: "Source Sans Pro", sans-serif;
    font-size: 16px;
    box-sizing: border-box;
  }
  .App {
    text-align: center;
    padding: 20px
  }
`;
