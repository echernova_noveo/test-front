import React, { Component } from 'react';
import {GlobalStyle} from '../../styledComponents/global';
import { Header } from '../../components/Header';
import { Content } from '../../components/Content';
import {StyledApp} from './App.style';

export class App extends Component {
  render() {
    return (
      <StyledApp>
        <GlobalStyle />
        <Header/>
        <Content>
          here your invoice manager ....
        </Content>
      </StyledApp>
    );
  }
}
