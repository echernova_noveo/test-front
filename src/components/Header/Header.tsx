import React from 'react';
import { StyledHeader } from './Header.style';
import logo from '../../assets/images/logo.svg';

export const Header: React.FunctionComponent = () => {
    return (
        <StyledHeader>
            <img src={logo} className="App-logo" alt="logo" />
        </StyledHeader>
    );
};
