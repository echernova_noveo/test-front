import styled from 'styled-components'

export const StyledHeader = styled.header`
  display: flex;
  justify-content: center;
  align-content: center;
  margin-bottom: 20px;
  padding: 20px;
  box-sizing: border-box;
  width: 100%;
`;
