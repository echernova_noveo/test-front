import styled from 'styled-components'
import { white, labor } from '../../const/colors';

export const StyledContent = styled.div`
  padding: 20px;
  text-align: left;
  box-sizing: border-box;
  background-color: ${white};
  color: ${labor};
  width: 100%;
  font-size: 1em;
  min-height: calc(100vh - 230px);
  border-radius: 5px
`;
