import React from 'react';
import { StyledContent } from './Content.style';

interface IPropsContent {
    children?: React.ReactNode;
}

export const Content: React.FunctionComponent<IPropsContent> = ({ children }) => {
    return (
        <StyledContent>
            {children}
        </StyledContent>
    );
};
